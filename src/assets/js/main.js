$(document).ready(function(){
	new Imager({ availableWidths: [320, 640, 1024] });

	FastClick.attach(document.body);

	$.slidebars();

	$('.slides_home').slick({
		dots:true,
		autoplay: true,
		arrows: false,
		speed: 2000,
		fade: true
	});

	//Sirve para activar los tabs al efecto hover.
	// $('.tab_menu_wines > li > a').hover(function() {
 	// 	$(this).tab('show');
	// });

	// Waypoints
	// var altura_header = $('header').height();
	// $('body').waypoint(function(direction) {		
	//   	if (direction === 'down') {
	//   		$('.line_vertical_gold').css('opacity', 1);
	// 	}
	// 	else{
	// 		$('.line_vertical_gold').css('opacity', 0);
	// 	}
	// }, {offset: -1});

});
 